package kr.co.hbilab.web;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * 2015. 5. 28.
 * @author user
 * 
 */
public class HelloController implements Controller{

    @Override
    public ModelAndView handleRequest(HttpServletRequest req, HttpServletResponse resp)
            throws Exception {
            ModelAndView mv = new ModelAndView();
            
            String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            
            mv.addObject("today", today);
            mv.setViewName("good");
        return mv;
    }
    
}
