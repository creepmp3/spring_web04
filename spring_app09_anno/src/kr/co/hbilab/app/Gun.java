package kr.co.hbilab.app;

public class Gun implements Weapon{
    int bullet;
    
    public Gun(){
        bullet = 6;
    }
    
    public int fire(){
        bullet--;
        System.out.println("빵 ~~~ ("+bullet+")");
        if(bullet==0) reload();
        
        return bullet;
    }
    
    public void reload(){
        bullet = 6;
        System.out.println("Reload ("+bullet+")");
    }

    @Override
    public void use() {
        fire();
    }

    @Override
    public void reuse() {
        reload();
    }

    @Override
    public void drop() {
        
    }
}
