/**
 * 2015. 6. 11.
 * @author KDY
 */
package kr.co.hbilab.dao;

import java.util.List;

import kr.co.hbilab.dto.BoardDTO;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

public class BoardDAO implements Dao {
    
    @Autowired
    SqlSession ss;
    
    @Override
    public List<BoardDTO> selectAll() {
        return ss.selectList("kr.co.hbilab.board.selectAll");
    }
    
    @Override
    public int insertOne(BoardDTO dto) {
        return ss.insert("kr.co.hbilab.board.insertOne", dto);
    }
    
    @Override
    public BoardDTO selectOne(int bno) {
        return ss.selectOne("kr.co.hbilab.board.selectOne", bno);
    }
    
    @Override
    public void readCountAdd(int bno) {
        ss.selectOne("kr.co.hbilab.board.readCountAdd", bno);
    }
    
    @Override
    public void hitsCountAdd(int bno) {
        ss.selectOne("kr.co.hbilab.board.hitsCountAdd", bno);
    }
    
}
