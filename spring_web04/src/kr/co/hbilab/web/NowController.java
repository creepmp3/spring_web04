package kr.co.hbilab.web;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * 2015. 5. 29.
 * @author KDY
 * 
 */
public class NowController implements Hello, Controller{

    @Override
    public ModelAndView handleRequest(HttpServletRequest req, HttpServletResponse resp)
            throws Exception {
        /*
        ModelAndView model = new ModelAndView();
        model.addObject("time", sayHello());
        model.setViewName("now");*/

        return new ModelAndView("now", "time", sayHello());
    }

    @Override
    public String sayHello() {
        String time = new SimpleDateFormat("HH시 mm분 ss초").format(new Date());
        return "현재 시간 : " + time;
    }
    
}
